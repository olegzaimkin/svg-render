const path = require('path');

module.exports = {
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'linebreak.js',
    library: 'linebreak',
    libraryTarget: 'umd',
    umdNamedDefine: true 
  },
  module: {
    rules: [
        {
            test: /\.(js)$/,
            loader: 'transform-loader?brfs'
        }
    ]
}
};
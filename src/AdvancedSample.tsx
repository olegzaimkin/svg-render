import React from 'react';
import { Component2 as SvgText2 } from './SvgText';
import { FontFactory, TextBlock, TextFormat } from './SvgText/types';

const loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';

const alignOptions: TextBlock['align'][] = ['left', 'right', 'center', 'justify'];
const wordWrapOptions: TextFormat['breakLine'][] = ['any', 'auto', 'none'];
const fontSizes: number[] = [9, 12, 14, 18, 24];

export default function AdvancedComponent({ fontFactory }: { fontFactory: FontFactory }) {
  const [textAligh, setTextAligh] = React.useState<TextBlock['align']>('left');
  const [fontSize, setFontSize] = React.useState<number>(14);
  const [wordWrap, setWordWrap] = React.useState<TextFormat['breakLine']>('auto');

  return (
    <div className="AdvancedSample">
      <div>
        <select value={textAligh} onChange={e => setTextAligh(e.target.selectedOptions[0].value as any)}>
			    {alignOptions.map(p => <option>{p}</option>)}
        </select>
        <select value={wordWrap} onChange={e => setWordWrap(e.target.selectedOptions[0].value as any)}>
			    {wordWrapOptions.map(p => <option>{p}</option>)}
        </select>
        <select value={fontSize} onChange={e => setFontSize(e.target.selectedOptions[0].value as any)}>
			    {fontSizes.map(p => <option>{p}</option>)}
        </select>
      </div>
      <div>
        <SvgText2 fonts={fontFactory} content={[{ align: textAligh, content: [
          { text: loremIpsum, format: { fontFamily: 'arial', fontSize, breakLine: wordWrap } },
        ] }]} width={6} height={2} />
      </div>
    </div>
  );
}

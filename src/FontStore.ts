import { FontDescriptor, FontFactory } from './SvgText/types';
import fontkit from '@pdf-lib/fontkit';

export class FontStore {
	_store: Record<string, fontkit.Font> = {}
	_defaultFont: FontDescriptor | null = null;

	addFont(descriptor: FontDescriptor, font: fontkit.Font): void {
		const fontKey = this.makeFontKey(descriptor);
		this._store[fontKey] = font;		
	}

	setDefaultFont(descriptor: FontDescriptor) {
		this._defaultFont = descriptor;
	}

	private makeFontKey({ fontFamily, style, weight }: FontDescriptor) : string {
		return `${fontFamily}#${style ?? 'regular'}#${weight ?? 'normal'}`;
	}

	factory(): FontFactory {
		return (arg) => {
			const descriptor = (typeof arg === 'string') ? { fontFamily: arg, style: undefined, weight: undefined} : arg;
			for(let probe of [
				descriptor,
				{ ...descriptor, weight: undefined },
				{ ...descriptor, style: undefined },
				{ ...descriptor, weight: undefined, style: undefined },
				this._defaultFont,
			]) {
				if (!probe) continue;
				const fontKey = this.makeFontKey(probe);
				
				const font = this._store[fontKey];
				if (font != null) return font;
			}
			return undefined as any as fontkit.Font;
		};
	}
}
import React, { useEffect } from 'react';
import fontkit from '@pdf-lib/fontkit';

import './App.css';
import { Component2 as SvgText2 } from './SvgText';
import AdvancedSample from './AdvancedSample';
import BenchSample from './BenchSample';
import { FontDescriptor, FontFactory, TextBlock } from './SvgText/types';
import { FontStore } from './FontStore';

const loadFontsList: (FontDescriptor & { url: string })[] = [
  { fontFamily: 'arial', url: 'arial.ttf' },
  { fontFamily: 'arial', style: 'italic', url: 'ariali.ttf' },
  { fontFamily: 'arial', weight: 'bold', url: 'arialbd.ttf' },
  { fontFamily: 'arial', style: 'italic', weight: 'bold', url: 'arialbi.ttf' },
  { fontFamily: 'centaur', url: 'CENTAUR.TTF' },
  { fontFamily: 'kosugimaru', url: 'KosugiMaru-Regular.ttf' },
];


const hairSpace = '\u200a';
const zeroSpace = '\u200b';
const diacritics = 'óçôñï ñẸ̀' + zeroSpace + 'W+'; // 🧑‍🍳
const loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';

type Sample = { title: string; content: TextBlock[], width: number, height: number };
const Samples: Sample[] = [
  { title: '', content: [{ content: [
    { text: "Hello world", format: { fontFamily: 'arial', fontSize: 16 } },
  ] }], width: 3, height: 0.4 },
  {
    title: 'Ligatures', content: [{ content: [
      { text: `LW L${zeroSpace}W (zerospace)`, format: { fontFamily: 'arial', fontSize: 18 } },
    ] }], width: 3, height: 0.6 },
  { title: 'Diacritics', content: [{ content: [
      { text: diacritics, format: { fontFamily: 'arial', fontSize: 16 } },
    ] }], width: 3, height: 0.4 },
  { title: 'CJK', content: [{ content: [
    { text: '仙台市大好き', format: { fontFamily: 'kosugimaru', fontSize: 14 } },
    ] }], width: 2, height: 0.4 },
  { title: 'Charwrap', content: [{ align: 'left', content: [
    { text: loremIpsum, format: { fontFamily: 'arial', fontSize: 14, breakLine: 'any' } },
    ] }], width: 4, height: 1.3 },
  { title: 'No wrap', content: [{ align: 'left', content: [
    { text: loremIpsum, format: { fontFamily: 'arial', fontSize: 14, breakLine: 'none' } },
    ] }], width: 4, height: 0.4 },
  { title: 'Multiple paragraphs', content: [
      { align: 'justify', content: [ { text: loremIpsum, format: { fontFamily: 'arial', fontSize: 14 } }, ] },
      { align: 'justify', content: [ { text: loremIpsum, format: { fontFamily: 'arial', fontSize: 14 } }, ] },
    ], width: 4, height: 3 },
  { title: 'Rich text', content: [{ align: 'justify', content: [
      { text: 'Hello   ', format: { fontFamily: 'arial', fontSize: 14 } },
      { text: 'world ', format: { fontFamily: 'centaur', fontSize: 22 } },
      { text: 'With ', format: { fontFamily: 'arial', fontSize: 12 } },
      { text: 'some italic, ', format: { fontFamily: 'arial', fontSize: 12, style: 'italic' } },
      { text: 'italic bold, ', format: { fontFamily: 'arial', style: 'italic', weight: 'bold', fontSize: 12 } },
      { text: 'and simply bold ', format: { fontFamily: 'arial', weight: 'bold', fontSize: 12 } },
      { text: 'text.', format: { fontFamily: 'arial', fontSize: 12 } },
    ] }], width: 3, height: 0.7 },
];

function App() {
  const fontFactory = React.useRef<FontFactory>();

  const [isFontLoaded, setFontLoaded] = React.useState(false);
  const [msg, setMsg] = React.useState('');

  async function fetchFont(fontName: string) {
    const response = await fetch(fontName);
    const buffer = await response.arrayBuffer();
    return fontkit.create(Buffer.from(buffer));
  }

  async function fetchFonts() {
    const fontStore: FontStore = new FontStore();
    let fontCount = 0, glyphCount = 0;
    for(let desc of loadFontsList) {
      try {
        const font = await fetchFont(desc.url);
        fontStore.addFont(desc, font);
        console.log(`Loaded font "${desc.fontFamily}" with ${font.numGlyphs} glyphs`);
        fontCount++;
        glyphCount += font.numGlyphs;
      }
      catch (e) {
        console.log(`Failed to load ${desc.fontFamily} font`, { e });
      }
    }

    // FIXME handle non-existing font, check case-sensitivity
    fontStore.setDefaultFont({ fontFamily: 'arial' });
    fontFactory.current = fontStore.factory();

    setMsg(`Loaded ${fontCount} fonts with total of ${glyphCount} glyphs`);
    setFontLoaded(true);
  }

  useEffect(() => { fetchFonts(); }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Rendering text via SVG/Fontkit.
        </p>

      </header>
      <main>
        { msg && <div>{msg}</div> }
        { isFontLoaded || <div>Loading font...</div> }
        { isFontLoaded && fontFactory.current != null
          && <>
            <h3>Samples:</h3> { Samples.map(s => <>
              <h3>{s.title}</h3>
              <SvgText2 fonts={fontFactory.current!} content={s.content} width={s.width} height={s.height} />
            </>) }
          <div>
            <table>
              <tr>
                <td colSpan={2}><h3>Multiline</h3></td>
              </tr>
              <tr>
                <td><SvgText2 fonts={fontFactory.current} content={[{ align: 'left', content: [
                    { text: loremIpsum, format: { fontFamily: 'arial', fontSize: 14 } },
                  ] }]} width={4} height={1.4} /></td>
                <td>
                  <div style={{ fontFamily: 'arial', fontSize: '14pt', width: '4in'}}>{loremIpsum}</div>
                </td>
              </tr>
            </table>
          </div>
          <h3>Advanced sample</h3>
          <AdvancedSample fontFactory={fontFactory.current} />
          <h3>Bench sample</h3>
          <BenchSample fontFactory={fontFactory.current} />
          </>
        }
      </main>
    </div>
  );
}

function diacriticsSample(font: fontkit.Font) {
  const run = font.layout(diacritics);
  console.log({ name: 'diacritics', textlen: diacritics.length, runlen: run.glyphs.length,
    advances: 	run.positions,
    glyphs: run.glyphs,
    text: diacritics.length,
    textn: diacritics.normalize().length,
  });
}

export default App;

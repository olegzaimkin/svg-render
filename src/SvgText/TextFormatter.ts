import { GlyphRun } from '@pdf-lib/fontkit';
import LineBreaker from 'linebreaker';
import { FontFactory, LineElement, TextBlock, TextLine, TextPiece, TextSpan } from "./types";

type CutFn = (size: number, isLineStart: boolean) => CutResult;

type CutResult = {
	type: 'complete', pieces: LineElement[]
} | {
	type: 'fail'
} | {
	type: 'success', pieces: LineElement[], tail: CutFn;
};

function sliceWords(chunks: TextPiece[], startRunIndex: number = 0): CutFn {
	return (width, isLineStart) => {
		if (startRunIndex >= chunks.length) return ({ type: 'complete', pieces: [] });

		const pieces: TextPiece[] = [];
		let remainingSpace = width;
		// console.log({ chunks });
		for(let i = startRunIndex; i < chunks.length; i++) {
			const piece = chunks[i];
			const pieceLength = piece.glyphrun.positions.reduce((len, position) => len + position.xAdvance, 0);
			// console.log({ remainingSpace, pieceLength });
			if (pieceLength > remainingSpace) {
				if (pieces.length === 0) {
					if (!isLineStart) return { type: 'fail' };	// make another try on the next line
					// forcefully push one piece to that line. TODO: try to put to a next line, TODO: split run
					pieces.push(piece);
				}
				break;
			}
			pieces.push(piece);
			remainingSpace -= pieceLength;
		}

		const nextIndex = pieces.length + startRunIndex;
		const piecesUp: LineElement[] = pieces.map(piece => ({ type: 'piece', ...piece, height: 0 }))
		return (nextIndex === chunks.length) ? { type: 'complete', pieces: piecesUp }
			: { type: 'success', pieces: piecesUp, tail: sliceWords(chunks, nextIndex) };
		// FIXME here's wrong assumption that glyphs correspond to text chars
	}
}

function *splitText(element: TextSpan, text: string, run: GlyphRun): Generator<TextPiece> {
	const linebreaker = new LineBreaker(text);
	let last = 0;
	let bk;
	while (bk = linebreaker.nextBreak()) {
	  // get the string between the last break and this one
	  const wordRun: GlyphRun = { ...run, glyphs: run.glyphs.slice(last, bk.position), positions: run.positions.slice(last, bk.position) };
	  const width = wordRun.positions.reduce((len, position) => len + position.xAdvance, 0);
	  yield { element, rawTextStart: last, rawTextLen: bk.position - last, scale: 0, glyphrun: wordRun, width, height: 0, pos: 0 };
	  last = bk.position;
	}
}

function sliceByChar(element: TextSpan, glyphrun: GlyphRun, startIndex: number = 0): CutFn {
	return (width, isLineStart) => {
		if (startIndex >= glyphrun.glyphs.length) return ({ type: 'complete', pieces: [] });

		let remainingSpace = width;
		let nextIndex = startIndex;
		for(; nextIndex < glyphrun.glyphs.length; nextIndex++) {
			const pieceLength = glyphrun.positions[nextIndex].xAdvance;
			if (pieceLength > remainingSpace) {
				if (nextIndex === startIndex) {
					if (!isLineStart) return { type: 'fail' };	// make another try on the next line
					// forcefully push one piece to that line. TODO: try to put to a next line, TODO: split run
					nextIndex++;
				}
				break;
			}
			remainingSpace -= pieceLength;
		}

		const lineRun: GlyphRun = { ...glyphrun, glyphs: glyphrun.glyphs.slice(startIndex, nextIndex), positions: glyphrun.positions.slice(startIndex, nextIndex) };
		const linePiece: LineElement = {
			type: 'piece',
			element, rawTextStart: startIndex, rawTextLen: nextIndex - startIndex, scale: 1,
			glyphrun: lineRun, width, height: 0, pos: 0,
		};
		if (nextIndex === glyphrun.glyphs.length) {
			return { type: 'complete', pieces: [linePiece] };
		}
		return {
			type: 'success', pieces: [linePiece], tail: sliceByChar(element, glyphrun, nextIndex)
		};
		// FIXME here's wrong assumption that glyphs correspond to text chars
	}
}

function sliceNoWrap(element: TextSpan, glyphrun: GlyphRun): CutFn {
	const slicer = sliceByChar(element, glyphrun, 0);
	return (width, _) => {
		const result = slicer(width, true);
		return (result.type === 'success') ? { type: 'complete', pieces: result.pieces } : result;
	}
}

/** Faster layout implementation for short and simple strings. */
function FastCut(element: TextSpan, run: GlyphRun, cutFn: CutFn, debug: boolean = false): CutFn {
	return (width, isLineStart) => {
		if (debug) console.log('FastCut->', { width })
		// shortcut: text fits the area, no justify
		const runLength = run.positions.reduce((len, position) => len + position.xAdvance, 0);
		if(runLength <= width) {
			if (debug) console.log('FastCut succeeded')
			return { type: 'complete', pieces: [{ type: 'piece', element, rawTextStart: 0, rawTextLen: element.text.length, scale: 0, glyphrun: run, pos: 0, width: runLength, height: 0 }] };
		}
		return cutFn(width, isLineStart);
	};
}


/** Calculates position of every piece within a line */
function alignPieces(line: TextLine, width: number, isLastLine: boolean) {
	const extraSpace = width - line.textWidth;

	let pos = 0;
	for(let a of line.atoms) {
		a.pos = pos;
		pos += a.width;
	}
	switch(line.block.align) {
		case 'center':
			line.atoms.forEach(a => a.pos += extraSpace/2);
			break;
		case 'right':
			line.atoms.forEach(a => a.pos += extraSpace);
			break;
		case 'justify':
				// do not justify last line
				if (line.atoms.length > 1 && (line.block.justify === 'all' || !isLastLine)) {
					// split equally
					const dx = extraSpace / (line.atoms.length - 1);
					line.atoms.forEach((a, i) => a.pos += dx * i);
				}
			break;
	}
}

export function formatTextBlock(getFont: FontFactory, block: TextBlock, width: number, debug: boolean = false): TextLine[] {
	const textLines: TextLine[] = [];
	const atoms: LineElement[] = [];
	let availableSpace = width;

	function flushTextLine() {
		const rawTextStart = Math.min(...atoms.map(a => (a.type === 'piece' ? a.rawTextStart : 0)));
		const rawTextEnd = Math.max(...atoms.map(a => (a.type === 'piece' ? a.rawTextStart + a.rawTextLen : 0)));
		const textWidth = atoms.reduce((len, a) => len + a.width, 0);
		const textHeight = Math.max(...atoms.map(a => (a.type === 'piece' ? a.height : 0)));
		textLines.push({ block, atoms: [...atoms], rawTextStart, rawTextLength: rawTextEnd - rawTextStart, textHeight, textWidth,  });
		atoms.splice(0, atoms.length);
		availableSpace = width;
	}

	// TODO preprocess block spans for substitution

	for (const span of block.content) {
		const font = getFont(span.format);
		const scale = span.format.fontSize / font.unitsPerEm;
		const lineHeight = font.bbox.maxY * scale;

		const run = font.layout(span.text);
		// let sliceFn = CutText(span, font, debug);
		let sliceFn = 
			span.format.breakLine === 'any' ? sliceByChar(span, run, 0) : 
			span.format.breakLine === 'none' ? sliceNoWrap(span, run) :
			sliceWords([...splitText(span, span.text, run)]);
		if(block.align != 'justify') {
			sliceFn = FastCut(span, run, sliceFn, debug);
		}

		if (debug) console.log('formatTextBlock', { scale, lineHeight, width });

		while(true) {
			const isLineStart = atoms.length === 0;
			const slice = sliceFn(availableSpace/scale, isLineStart);
			// console.log({ slice });
			if (slice.type === 'fail') {
				if (atoms.length === 0 && isLineStart) {
					// TODO failure
					console.log('failed to put any elements to a line');
					break;
				} else {
					flushTextLine();
				}
			}
			else {
				const pieces = slice.pieces.map(p => ({...p, scale, height: lineHeight, width: p.width * scale }));
				atoms.push(...pieces);
				const piecesLen = pieces.reduce((len, p) => len + p.width, 0);
				availableSpace -= piecesLen;

				if(debug) console.log({ pieces, piecesLen, availableSpace });

				if(slice.type === 'complete') {
					break;
				}
				else {
					sliceFn = slice.tail;
				}
			}
		}
	}
	if(atoms.length > 0) flushTextLine();
	textLines.forEach((line, idx) => alignPieces(line, width, idx === textLines.length - 1));

	return textLines;
}

import { Font, GlyphRun } from "@pdf-lib/fontkit";

export type TextFormat = {
    fontFamily: string;
    fontSize: number;
    // cssClass?: string; // the class to be set to resulting SVG element (define colors)
    breakLine?: 'auto' | 'any' | 'none'; // word/char/none wrapping
    charSpacing?: number;
    style?: 'italic'
    weight?: 'bold'
};

export type TextSpan = {
    text: string;
    format: TextFormat;
};

export type TextBlock = {
    content: TextSpan[];
    align?: 'left' | 'center' | 'right' | 'justify';
    justify?: 'normal' | 'all';
    rotation?: '0' | '90';
    lineSpacing?: number;
};

export type FontDescriptor = Pick<TextFormat, 'fontFamily' | 'style' | 'weight'>;
export type FontFactory = (font: string | FontDescriptor) => Font;

/** layout result type */
export type TextPiece = {
	element: TextSpan;
	rawTextStart: number;
	rawTextLen: number;
    /** x-position within a line */
    pos: number;
    width: number;
    height: number;
	scale: number;
	glyphrun: GlyphRun;
}

export type TextSpace = {
    pos: number;
	width: number;
	glyphs: GlyphRun;
}

export type LineElement = { type: 'piece' } & TextPiece | { type: 'space' } & TextSpace;

/** Represents text formatting result */
export type TextLine = {
    block: TextBlock;
    atoms: LineElement[];
    rawTextStart: number;
    rawTextLength: number;
    textHeight: number;
    textWidth: number;
};

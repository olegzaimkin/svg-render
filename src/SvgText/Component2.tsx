import { Glyph } from '@pdf-lib/fontkit';
import { formatTextBlock } from './TextFormatter';
import { FontFactory, TextBlock } from './types';

export type Svg2TextProps = {
	fonts: FontFactory;
	content: TextBlock[];
	/** Text area width (in inches) */
	width: number;
	/** Text area height (inches) */
	height: number;
	debug?: boolean;
};

type OutItem = { glyph: Glyph, scale: number, x: number, y: number };

export const Component2 = ({ fonts, content: paragraphs, width, height, debug }: Svg2TextProps): JSX.Element => {

	const items: OutItem[] = [];
	let yPos = 0;

	for(let content of paragraphs) {
		const textLines = formatTextBlock(fonts, content, width * 72, debug);
		if(debug) console.log({ textLines });
		for(let textLine of textLines) {
			yPos += textLine.textHeight;
			for(let atom of textLine.atoms) {
				if(atom.type === 'piece') {
					const { glyphrun: { glyphs, positions }, scale } = atom;
					for(let index = 0, pos = 0; index < glyphs.length; index++) {
						const { yOffset, xOffset, xAdvance } = positions[index];
						items.push({ glyph: glyphs[index], scale, x: atom.pos + (pos - xOffset) * scale, y: yPos - yOffset * scale })
						pos += xAdvance;
					}
				}
			}
		}
	}

	return <div className='SvgText'>
		<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
			width={`${width}in`} height={`${height}in`}
			viewBox={`0 0 ${width * 72} ${height * 72}`}
			preserveAspectRatio="xMidYMid meet">
			{ items.map(({ glyph, scale, x, y }) => {
				return <g transform={`translate(${x},${y}) scale(${scale}, ${-scale})`}>
					<path d={glyph.path.toSVG()} />
				</g>;
			})}
		</svg>
		<div className="textLayer" style={{ top: `0` }}>
			<span>TODO render element to HTML</span>
		</div>
	</div>;
};

import React from 'react';
import { Component2 as SvgText2 } from './SvgText';
import { FontFactory, TextBlock } from './SvgText/types';

const sampleCounts: number[] = [50, 100, 500, 2000, 5000];

export default function BenchSample({ fontFactory }: { fontFactory: FontFactory }) {
  const [sampleCount, setSampleCount] = React.useState<number>(50);

  const samples: TextBlock[] = [];
  for(let i = 0; i < sampleCount; i++) {
    samples.push({ align: 'right', content: [
      { text: `Abc ${Math.round(Math.random() * 10000000) / 100}`, format: { fontFamily: 'arial', fontSize: 12, breakLine: 'auto' } },
    ] });
  }
  return (
    <div className="BenchSample">
      <div>
        <select value={sampleCount} onChange={e => setSampleCount(e.target.selectedOptions[0].value as any)}>
			    {sampleCounts.map(p => <option>{p}</option>)}
        </select>
      </div>
        { samples.map(s => <span>
          <SvgText2 fonts={fontFactory} content={[s]} width={1.4} height={0.3} />
        </span>)}
    </div>
  );
}

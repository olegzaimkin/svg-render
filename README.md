# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Project highlights

TODOs:
* [x] load multiple fonts
* [x] full processing in formatter
* [ ] render HTML side-by-side
* [ ] hanging spaces  - use .trimEnd() to identify the ending spaces
* [ ] measure performance (1-100 complex pages)

Features:
* [x] align text (left, center, right)
* [x] justify
* [x] CJK
* [x] char wrap/word wrap
* [x] multiple blocks
* [x] decoration (italic, bold)
* [ ] vertical text
* [ ] rotation 90
* [ ] underline, overline
* [ ] font substitute - process the span for codepoints presense
* [ ] justify: distribute - distribute space between glyphs not words
* [ ] character spacing - just add space to xAdvance if it's > 0
* [ ] fit width - scale horizontally before output
* [ ] text selection - requires HTML render

## How to build

* build the linebreaker-brfs project
  * `cd linebreaker-brfs`
  * `yarn build`
* install dependencies
  * `yarn`
* run the project
  * `yarn start`

## Running the sample

* copy (from windows/fonts folder) to ./public folder the following fonts: arial.ttf, arialbd.ttf, arialbi.ttf, ariali.ttf
